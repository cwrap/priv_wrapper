#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>

#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

static int setup(void **state)
{
	(void) state; /* unused */

	setenv("PRIV_WRAPPER", "1", 1);

	return 0;
}

static void test_chroot(void **state)
{
	int rc;

	(void) state; /* unused */

	setenv("PRIV_WRAPPER_CHROOT_DISABLE", "1", 1);
	rc = chroot("non existing path");
	unsetenv("PRIV_WRAPPER_CHROOT_DISABLE");

	assert_return_code(rc, errno);
}

static void test_chroot_fail(void **state)
{
	int rc;

	(void) state; /* unused */

	rc = chroot("non existing path");

	assert_int_equal(rc, -1);
	/*
	 * Linux: ENOENT
	 * FreeBSD: EPERM
	 */
	assert_in_range(errno, EPERM, ENOENT);
}

int main(void)
{
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_chroot),
		cmocka_unit_test(test_chroot_fail),
	};

	return cmocka_run_group_tests(tests, setup, NULL);
}
